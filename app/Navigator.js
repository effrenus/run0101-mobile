// @flow
import React, { Component } from 'react';
import { 
  Navigator 
} from 'react-native';
import TabsView from './containers/TabsView';
import SaveForm from './containers/SaveForm';
import About from './containers/About';
import Settings from './containers/Settings';
import LastActivities from './containers/LastActivities'

const Scenes = {
  SaveForm, About, Settings, LastActivities
};

class Navigation extends Component {
  renderScene (route, navigator) {
    let { scene, passProps } = route;
    if (scene === 'Home') {
      return <TabsView route={route} navigator={navigator} />;
    }
    
    const Komponent = route.component ? route.component : Scenes[scene];
    
    return (
      <Komponent route={route} navigator={navigator} {...passProps}  />
    );
  }
  
  render () {
    return (
      <Navigator
        initialRoute={{ scene: 'Home' }}
        renderScene={this.renderScene}
      />
    )
  }
}

export default Navigation;