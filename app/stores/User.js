import { AsyncStorage } from 'react-native';
import { observable } from 'mobx';

export default class User {
  @observable lastname = null;
  @observable firstname = null;
  @observable email = null;
  @observable gender = null;
  
  toObject () {
    const { lastname, firstname, email, gender } = this;
    return { lastname, firstname, email, gender };
  }
  
  toJSON () {
    return JSON.stringify(this.toObject());
  }
  
  load (data) {
    ['firstname', 'lastname', 'email', 'gender'].forEach(name => this[name] = data[name]);
  }
}