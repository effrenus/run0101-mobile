import React from 'react';
import { AppState } from 'react-native';
import { observable, action, computed } from 'mobx';
import { calculateDistance } from 'app/utils';
import Timer from 'app/utils/Timer';

export const ACTIVITY_TYPES: { [type: string]: { key: number } } = {
  running: { key: 1 },
  other: { key: 2 }
};

type Coordinate = [number, number];

type Coordinates = Array<Coordinate>;

type ExportActivity = {
  id: string,
  type: number,
  timeStarted: Date,
  duration: number,
  distance: number,
  comment: strung,
  coords: Coordinates,
  photo: ?{
    uri: string,
    type: string,
    name: strings
  }
};

export default class Activity {
  id: string = `$id${(new Date()).getTime()}`;
  @observable type: number = ACTIVITY_TYPES.running.key;
  @observable duration: number = 0;
  @observable distance: number = 0;
  @observable comment: string = '';
  coords: Coordinates = observable.shallowArray([]);
  @observable photo: ?string = null;
  inited: boolean = false;
  
  constructor () {
    this.timer = new Timer();
    this.timer.on('timeChanged', this.updateTime);
  }
  
  init () {
    this.inited = true;
    this.timeStarted = new Date();
    navigator.geolocation.getCurrentPosition(
      ({ coords: { latitude, longitude } }) => this.setStartPosition([latitude, longitude])
    );
  }
  
  start () {
    if (!this.inited) {
      this.init();
    }
    this.watchId = navigator.geolocation.watchPosition(
      ({ coords: { latitude, longitude } }) => this.updatePosition([latitude, longitude]),
      null,
      { enableHighAccuracy: true, distanceFilter: 10 }
    );
    this.timer.start();
  }
  
  stop () {
    this.timer.stop();
    navigator.geolocation.clearWatch(this.watchId);
    this.watchId = null;
  }
  
  toObject () {
    const { id, type, duration, coords, distance, photo, timeStarted, comment } = this;
    return {
      id,
      type,
      timeStarted,
      duration,
      distance,
      comment,
      coords: coords.toJS(),
      photo: photo ? {
        uri: photo,
        type: 'image/jpeg',
        name: `photo.jpg`
      } : null
    };
  }
  
  toJSON (): string {
    const object = this.toObject();
    
    return JSON.stringify(object);
  }
  
  @computed
  get pace () {
    return this.duration > 0 ? (this.distance / (this.duration / 60) ).toFixed(2) : 0;
  }
  
  @action
  updatePosition = ([latitude, longitude]) => {
    this.coords.push([latitude, longitude]);
    this.updateDistance();
  }
  
  @action
  setStartPosition (coords) {
    this.coords.push(coords);
    this.startPosition = coords;
  }
  
  @action.bound
  updateDistance () {
    const len = this.coords.length;
    const [start, end] = [this.coords[len - 1], this.coords[len - 2]];
    this.distance += parseFloat(calculateDistance(start, end).toFixed(1));
  }
  
  @action.bound
  updateTime (time) {
    this.duration = time;
  }
};