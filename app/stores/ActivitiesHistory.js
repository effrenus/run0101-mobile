import { observable } from 'mobx';
import { AsyncStorage } from 'react-native';

export default class ActivitiesHistory {
  @observable activities = [];
  
  append (activity) {
    this.activities.push(activity.toObject());
  }
  
  load (data) {
    this.activities = data;
  }
  
  remove (id) {
    this.activities = this.activities.filter(activity => activity.id !== id);
    return true;
  }
  
  clearAll () {
    this.activities = [];
  }
}
