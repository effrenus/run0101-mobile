import { AsyncStorage } from 'react-native';
import { observable, action } from 'mobx';
import User from './User';
import ActivitiesHistory from './ActivitiesHistory';

class Appstate {
  activities = new ActivitiesHistory();
  @observable user = new User();
  
  async loadStorage () {
    const storage = JSON.parse(await AsyncStorage.getItem('storage'));
    if (!storage) {
      return;
    }
    
    if (storage.user) {
      this.user.load(storage.user);
    }
    
    if (storage.activities) {
      this.activities.load(storage.activities);
    }
  }
  
  handleStateChange = async (state) => {
    if (state !== 'active') {
      await AsyncStorage.setItem('storage', JSON.stringify({
        user: this.user.toObject(),
        activities: this.activities.activities
      }));
    }
  }
}

export default new Appstate();