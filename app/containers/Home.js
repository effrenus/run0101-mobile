import React, { Component, PropTypes } from 'react';
import { View, Text } from 'react-native';
import { observer } from 'mobx-react/native';
import { inject } from 'mobx-react';
import Icon from 'react-native-vector-icons/MaterialIcons';

import ActivityPage from './Activity';
import appstate from 'app/stores';
import Activity from 'app/stores/Activity';
import * as config from 'app/config';
import { i18n } from 'app/i18n';

export default class Home extends Component {
  render () {
    return (
      <View style={{ flex: 1 }}>
        <Icon.ToolbarAndroid
          title={i18n.messages['toolbar.activity']}
          actions={[{ title: i18n.messages['toolbar.reset'] }]}
          style={{ height: config.TOOLBAR_HEIGHT, backgroundColor: config.BLUE_COLOR }}
          titleColor="#FFF"
          navIconName="menu"
          onIconClicked={() => this.context.openDrawer()}
        />
        <ActivityPage activity={new Activity()} navigator={this.props.navigator} />
      </View>
    );
  }
}

Home.contextTypes = {
  openDrawer: PropTypes.func
};
