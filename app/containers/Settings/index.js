// @flow
import React, { Component } from 'react';
import { View } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { inject } from 'mobx-react';
import SettingField from 'app/components/SettingField';
import * as config from 'app/config';
import { setLocale, i18n } from 'app/i18n';
import type { SelectorValue } from 'app/components/SettingField/SettingSelector';

// $FlowFixMe
@inject('appstate')
class Settings extends Component {
  handleLangChange = (value: SelectorValue) => {
    setLocale(String(value));
  }

  render () {
    const { appstate, navigator } = this.props;
    
    return (
      <View style={{ flex: 1, backgroundColor: '#FFF' }}>
        <Icon.ToolbarAndroid
          title={i18n.messages['toolbar.settings']}
          actions={[]}
          style={{ height: config.TOOLBAR_HEIGHT, backgroundColor: config.BLUE_COLOR }}
          titleColor="#FFF"
          navIconName="arrow-back"
          onIconClicked={() => this.props.navigator.pop()}
        />
        <SettingField 
          onChange={this.handleLangChange} 
          name={i18n.messages['settings.lang']}
          values={[{val: 'en', name: 'English'}, {val: 'ru', name: 'Русский'}]} 
          defaultValue={i18n.lang}
          navigator={navigator} />
      </View>
    );
  }
}
  
export default Settings;