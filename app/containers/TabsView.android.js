import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { BLUE_COLOR } from 'app/config';
import { i18n } from 'app/i18n';

import DrawerLayout from '../components/DrawerLayout';
import Home from './Home';

const Scenes = { Home };

class TabsView extends Component {
  onItemClicked (scene) {
    const { navigator } = this.props;
    navigator.push({ scene });
    this._drawer.closeDrawer();
  }
  
  renderNavigationView = () => {
    return (
      <View style={{ flex: 1 }}>
        <View style={styles.header}>
          <Text style={styles.headerText}>{i18n.messages['title']}</Text>
        </View>
        <TouchableOpacity onPress={this.onItemClicked.bind(this, 'LastActivities')}>
          <View style={styles.itemWrap}>
            <Icon name="assignment" size={30} style={styles.itemIcon} /> 
            <Text style={styles.itemText}>{i18n.messages['drawer.lastactivities']}</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={this.onItemClicked.bind(this, 'Settings')}>
          <View style={styles.itemWrap}>
            <Icon name="settings" size={30} style={styles.itemIcon} />
            <Text style={styles.itemText}>{i18n.messages['drawer.settings']}</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={this.onItemClicked.bind(this, 'About')}>
          <View style={styles.itemWrap}>
              <Icon name="info" size={30} style={styles.itemIcon} />
              <Text style={styles.itemText}>{i18n.messages['drawer.about']}</Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
  
  renderContent () {
    const { navigator, route } = this.props;
    const Komponent = Scenes[route.scene];
    
    return <Komponent navigator={navigator} />;
  }
  
  render () {
    return (
      <DrawerLayout
        ref={(ref) => this._drawer = ref}
        renderNavigationView={this.renderNavigationView}
      >
        {this.renderContent()}
      </DrawerLayout>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    height: 100,
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 15,
    backgroundColor: BLUE_COLOR
  },
  headerText: {
    color: '#FFF',
    fontFamily: 'Lobster',
    fontSize: 30
  },
  itemWrap: {
    flexDirection: 'row', 
    alignItems: 'center', 
    marginLeft: 10,
    marginBottom: 15,
    paddingLeft: 30 
  },
  itemText: {
    margin: 10,
    fontSize: 22
  },
  itemIcon: {
    position: 'absolute', 
    top: 8
  }
});

export default TabsView;