import React, { PureComponent } from 'react';
import { View, Alert, Text, ScrollView, StyleSheet, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { inject } from 'mobx-react';
import { observer } from 'mobx-react/native';
import { formatTime, formatDate } from 'app/utils';
import * as config from 'app/config';
import { i18n } from 'app/i18n';

@inject('appstate') @observer
class LastActivities extends PureComponent {
  handleDelete = (id) => {
    const { appstate: { activities } } = this.props;
    
    Alert.alert(
      i18n.messages['alert.confirm'],
      i18n.messages['alert.activity.delete'],
      [
        {text: i18n.messages['alert.cancel'], style: 'cancel'},
        {text: i18n.messages['alert.ok'], onPress: () => {
          activities.remove(id)
        }},
      ],
      { cancelable: false }
    )
  }
  
  render () {
    const { appstate: { activities } } = this.props;
    
    return (
      <ScrollView style={{ flex: 1, backgroundColor: '#FFF' }}>
        <Icon.ToolbarAndroid 
          title={i18n.messages['toolbar.lastactivities']}
          titleColor="#FFF"
          navIconName="arrow-back"
          onIconClicked={() => this.props.navigator.pop()} 
          iconColor="#FFF" 
          backgroundColor={config.BLUE_COLOR} 
          style={{ height: config.TOOLBAR_HEIGHT }} />
          
        {activities.activities.map((activity, i) => 
          <View key={`history-${i}`} style={styles.item}>
            <Text>{formatDate(activity.timeStarted)}</Text>
            <Text>{i18n.messages[`activity.type.${activity.type}`]}</Text>
            <Text>{activity.distance} km</Text>
            <TouchableOpacity onPress={() => this.handleDelete(activity.id)}>
              <Icon name="delete" size={23} color="#ff0000" />
            </TouchableOpacity>
          </View>)}
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  item: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 15,
    borderBottomWidth: 1,
    borderBottomColor: '#DDD'
  }
});

export default LastActivities;