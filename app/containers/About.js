import React, { Component } from 'react';
import { View, ScrollView } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import * as config from 'app/config';
import { H2, P } from 'app/components/Tags';
import { i18n } from 'app/i18n';

class About extends Component {
  render () {
    return (
      <View style={{ flex: 1, backgroundColor: '#FFF' }}>
        <Icon.ToolbarAndroid 
          title={i18n.messages['toolbar.about']}
          titleColor="#FFF"
          navIconName="arrow-back"
          onIconClicked={() => this.props.navigator.pop()} 
          iconColor="#FFF" 
          backgroundColor={config.BLUE_COLOR} 
          style={{ height: config.TOOLBAR_HEIGHT }} />
        <ScrollView style={{ padding: 10 }}>
          <H2>The essence</H2>
          <P>
            Runs of any distance at 1 january local time. Anywhere on the planet. Alone, with friends or on one of the organised races. After a run — write down the kilometers to the common Fund for run0101.com and share your photos and experiences on Facebook. Enjoy life
          </P>
          <H2>Philosophy</H2>
          <P>
            We run, not in order to set an example for others. We run for ourselves — to give ourselves the joy of running and socialising with friends. To give yourself a good mood.
          </P>
          <H2>Rules</H2>
          <P>In count only the miles that you ran 1 January from 00:00:00 to 23:59:59 local time. To make the kilometers, only 1 and 2 January Moscow time. Add kilometers on 3 January anymore.</P>
          <P>You can make kilometers for myself and for some friends.</P>
          <P>From the list without warning about deleted records, records with no real name and names and entries containing religious, political and any other slogans.</P>
        </ScrollView>
      </View>
    );
  }
}

export default About;