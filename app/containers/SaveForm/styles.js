import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  saveButton: {
    position: 'absolute',
    left: 0,
    bottom: 0,
    right: 0,
    height: 55,
    backgroundColor: 'green',
    alignItems: 'center',
    justifyContent: 'center'
  },
  field: {
    flexDirection: 'row', 
    alignItems: 'center',
    justifyContent: 'space-between', 
    padding: 15, 
    borderBottomWidth: 1, 
    borderBottomColor: '#EEE'
  },
  saveButtonText: {
    fontSize: 25,
    color: '#FFF'
  },
  text: {
    fontSize: 18,
    color: '#000'
  }
});