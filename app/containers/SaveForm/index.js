import React, { Component } from 'react';
import { 
  View, 
  Text, 
  ToolbarAndroid, 
  StatusBar, 
  ScrollView, 
  StyleSheet, 
  TouchableHighlight,
  TextInput,
  Image
} from 'react-native';
import Camera from 'react-native-camera';
import { observer } from 'mobx-react/native';
import { inject } from 'mobx-react';
import ModalPicker from 'app/components/ModalPicker';
import CapturePhoto from 'app/components/CapturePhoto';
import { TOOLBAR_HEIGHT, BLUE_COLOR } from 'app/config';
import Error from 'app/components/Error';
import { formatTime } from 'app/utils';
import styles from './styles';
import { i18n } from 'app/i18n';

const TOOLBAR_ACTIONS = [
  { title: i18n.messages['toolbar.delete'], action: 'delete' }
];

@inject("appstate") @observer
class SaveForm extends Component {
  state = {
    errors: {}
  };
  
  constructor () {
    super();
    this.updateLastname = this.updateUserData.bind(this, 'lastname');
    this.updateFirstname = this.updateUserData.bind(this, 'firstname');
    this.updateEmail = this.updateUserData.bind(this, 'email');
  }
  
  onCapture = (path) => this.props.activity.photo = path;
  
  onActionSelected = (index) => {
    const { action } = TOOLBAR_ACTIONS[index];
    switch (action) {
      case 'delete':
        this.props.navigator.replace({ scene: 'Home' });
        break;
    }
  }
  
  updateUserData (name, value) {
    this.props.appstate.user[name] = value;
  }
  
  handleSave = async () => {
    const { appstate: { locale, activities }, activity } = this.props;
    const user = this.props.appstate.user;
    const errors = this.validate();
    
    if (Object.keys(errors).length === 0) {
      activities.append(activity);
      
      const formData = new FormData();
      const userData = {
        ...user.toObject(),
        ...activity.toObject()
      };
      userData.coords = JSON.stringify(userData.coords);
      
      for (const field of Object.keys(userData)) {
        formData.append(field, userData[field]);
      }
      
      fetch('http://192.168.0.103:3000/api/v1/activity/', {
        method: 'POST',
        headers: {
          'Content-type': 'multipart/form-data'
        },
        body: formData
      })
        .then(res => res.json())
        .then(text => console.log(text))
        .catch(err => alert(err));
    } else {
      this.setState(() => ({ errors }))
    }
  }
  
  validate () {
    const { activity, appstate: { user } } = this.props;
    const errors = {};
    ['lastname', 'firstname', 'email'].filter(name => !user[name]).forEach(name => errors[name] = `Field ${name} is required.`);
    if (activity.distance < 0 || activity.distance > 100) {
      errors['distance'] = 'Distance must be more 1 km and less 100 km.';
    }
    
    return errors;
  }
  
  render () {
    const { activity, appstate } = this.props;
    const user = this.props.appstate.user;
    const { errors } = this.state;
    
    return (
      <View style={{ flex: 1, backgroundColor: '#EEE' }}>
        <ToolbarAndroid
          title={i18n.messages['saveform.toolbar']}
          actions={TOOLBAR_ACTIONS}
          onActionSelected={this.onActionSelected}
          style={{ height: TOOLBAR_HEIGHT, backgroundColor: BLUE_COLOR }}
          titleColor="#FFF"
        />
        <ScrollView style={{ flex: 1, marginBottom: 55 }}>
          <View style={{ backgroundColor: '#FFF', marginBottom: 20 }}>
            <View style={styles.field}>
              <Text style={styles.text}>{i18n.messages['activity.duration']}</Text>
              <Text style={styles.text}>{formatTime(activity.duration)}</Text>
            </View>
            
            <View style={styles.field}>
              <Text style={styles.text}>{i18n.messages['activity.distance']}</Text>
              <Text style={styles.text}>{activity.distance.toFixed(2)} KM</Text>
            </View>
            <Error text={errors.distance} />
            
            <View style={styles.field}>
              <Text style={styles.text}>{i18n.messages['activity.type']}</Text>
              <ModalPicker 
                initValue={i18n.messages['activity.type.1']} 
                data={[{ key: 1, label: i18n.messages['activity.type.1'] }, { key: 2, label: i18n.messages['activity.type.2'] }]} />
            </View>
            
            <View style={styles.field}>
              <Text style={styles.text}>{i18n.messages['activity.photo']}</Text>
              {activity.photo 
                ? <Image key={activity.photo} source={{ uri: activity.photo, isStatic: true }} style={{ width: 65, height: 65 }} />
                : <CapturePhoto onCapture={this.onCapture} /> }
              
            </View>
          </View>
          
          <View style={{ backgroundColor: "#FFF" }}>
            <View style={styles.field}>
              <TextInput 
                placeholder={i18n.messages['user.lastname']} 
                value={user.lastname}
                onChangeText={this.updateLastname} 
                underlineColorAndroid="#FFF" style={{ width: 135, height: 40, fontSize: 18, padding: 0, borderBottomWidth: 1, borderBottomColor: '#CCC' }} />
              <TextInput 
                placeholder={i18n.messages['user.firstname']}
                value={user.firstname}
                onChangeText={this.updateFirstname} 
                underlineColorAndroid="#FFF" style={{ width: 135, height: 40, fontSize: 18, padding: 0, borderBottomWidth: 1, borderBottomColor: '#CCC' }} />
            </View>
            <Error text={errors.lastname || errors.firstname ? 'Lastname and firstname are required' : ''} />
            
            <View style={styles.field}>
              <Text style={styles.text}>{i18n.messages['user.gender']}</Text>
              <ModalPicker 
                onChange={val => user.gender = val.key} 
                initValue={user.gender ? (user.gender == 1 ? 'Female' : 'Male') : 'Select'} 
                data={[{ key: 1, label: 'Female' }, { key: 2, label: 'Male' }]} />
            </View>
            
            <View style={styles.field}>
              <Text style={styles.text}>{i18n.messages['user.email']}</Text>
              <TextInput 
                onChangeText={this.updateEmail} 
                value={user.email}
                underlineColorAndroid="#FFF" style={{ width: 150, height: 40, fontSize: 18, padding: 0, textAlign: 'right', borderBottomWidth: 1, borderBottomColor: '#CCC' }} />
            </View>
            <Error text={this.state.errors.email} />
          </View>
        </ScrollView>
        <TouchableHighlight onPress={this.handleSave} style={styles.saveButton}>
          <Text style={styles.saveButtonText}>Save</Text>
        </TouchableHighlight>
      </View>
    );
  }
}

export default SaveForm;