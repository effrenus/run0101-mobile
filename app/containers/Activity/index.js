import React, { Component, PropTypes } from 'react';
import { 
  View, 
  StyleSheet, 
  Text, 
  TouchableHighlight, 
  Alert, 
  ScrollView,
  StatusBar,
  Animated } from 'react-native';
import { observer } from 'mobx-react/native';
import { inject } from 'mobx-react';
import Map from 'app/components/Map';
import Timer from 'app/components/Timer';
import Field from 'app/components/Field';
import styles from './styles';
import { i18n } from 'app/i18n';
import { BLUE_COLOR } from 'app/config';

@inject('appstate') @observer
class ActivityPage extends Component {
  state = {
    active: false,
    inited: false,
    buttonWidthAnim: new Animated.Value(0)
  };
  
  toggleActiveState = () => {
    if (!this.isPending) {
      this.isPending = true;
      const active = !this.state.active;
      const { activity } = this.props;
      active ? activity.start() : activity.stop();
      this.setState({ active });
      
      if (!this.state.inited) {
        this.onInitAnimation(() => {
          this.isPending = false;
          this.setState(() => ({ inited: true }));
        });
      } else {
        this.isPending = false;
      }
      
    }
  }
  
  handleStop = () => {
    Alert.alert(
      i18n.messages['alert.confirm'],
      i18n.messages['alert.activity.stop'],
      [
        {text: i18n.messages['alert.cancel'], style: 'cancel'},
        {text: i18n.messages['alert.ok'], onPress: () => {
          const { navigator, activity } = this.props;
          this.props.activity.stop();
          navigator.replace({ 
            scene: 'SaveForm',
            passProps: { activity: this.props.activity }
          });
        }},
      ],
      { cancelable: false }
    );
  }
  
  onInitAnimation (cb = () => {}) {
    Animated.timing(
      this.state.buttonWidthAnim,
      {
        toValue: 1,
        duration: 500
      }
    ).start(cb);
  }
  
  render () {
    const { activity, appstate } = this.props;
    
    return (
      <View style={styles.container}>
        <View style={styles.activityData}>
          <View>
            <Timer activity={activity} label={i18n.messages['activity.duration']} />
            
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingRight: 10 }}>
              <Field label={i18n.messages['activity.distance']} value={`${Number(activity.distance).toFixed(2)}`} unit="km" />
              <Field label={i18n.messages['activity.pace']} value={activity.pace} unit="km/min" />
            </View>
          </View>
          
          <View style={{ flex: 1, paddingBottom: 50 }}>
            <Map coords={activity.coords} />
          </View>
        </View>
        
        <View style={styles.controls}>
          {!this.state.inited && this.state.active || this.state.inited
            ? <Animated.View style={[styles.button, styles.stop, { flex: this.state.buttonWidthAnim.interpolate({
                 inputRange: [0, 1],
                 outputRange: [0, 0.5]
               }) }]}>
                <TouchableHighlight onPress={this.handleStop} style={styles.buttonLabelWrap}>
                  <Animated.View style={{ opacity: this.state.buttonWidthAnim }}><Text style={styles.buttonText}>{i18n.messages['ctrl.stop']}</Text></Animated.View>
                </TouchableHighlight>
              </Animated.View>
            : null
          }
          <Animated.View 
            style={[styles.button, styles.start, { flex: this.state.buttonWidthAnim.interpolate({
               inputRange: [0, 1],
               outputRange: [1, 0.5]
             }) }]}>
            <TouchableHighlight onPress={this.toggleActiveState} style={styles.buttonLabelWrap}>
              <Text style={styles.buttonText}>{this.state.active ? i18n.messages['ctrl.pause'] : i18n.messages['ctrl.start']}</Text>
            </TouchableHighlight>
          </Animated.View>
        </View>
      </View>
    );
  }
}

export default ActivityPage;