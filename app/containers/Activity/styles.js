import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF'
  },
  activityData: {
    flex: 1, 
    paddingTop: 10
  },
  controls: {
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    flexDirection: 'row'
  },
  buttonLabelWrap: { 
    position: 'absolute', 
    left: 0, 
    top: 0, 
    bottom: 0, 
    right: 0,
    justifyContent: 'center',
    alignItems: 'center'
  },
  button: {
    height: 50
  },
  buttonText: {
    color: '#FFF', 
    fontSize: 25, 
    fontFamily: 'Lobster'
  },
  start: {
    backgroundColor: 'green'
  },
  stop: {
    backgroundColor: 'crimson'
  }
});