import { NativeModules } from 'react-native';
import { observable, action } from 'mobx';
import { observer } from 'mobx-react/native';
import { SUPPORTED_LANG } from 'app/config';
import en from './en';
import ru from './ru';

const i18nMessages = {
  en, ru
};

const sysLang = NativeModules.I18nManager.localeIdentifier.split('_')[0];
const defaultLang = isSupported(sysLang) ? sysLang : 'en';

export const i18n = observable({ 
  lang: isSupported(defaultLang) ? defaultLang : 'en',
  messages: i18nMessages[defaultLang]
});

export function setLocale (lang) {
  if (isSupported(lang) && i18n.lang !== lang) {
    i18n.lang = lang;
    i18n.messages = i18nMessages[lang];
  }
}

function isSupported (lang) {
  return SUPPORTED_LANG.includes(lang);
}

// class Locale {
//   @observable locale = null;
//   
//   constructor () {
//     const systemLang = NativeModules.I18nManager.localeIdentifier.split('_')[0];
//     this.setLocale(this.isSupported(systemLang) ? systemLang : 'en');
//   }
//   
//   @action
//   setLocale (value) {
//     value = value.split('_')[0];
//     if (this.isSupported(value) && this.locale !== value) {
//       this.locale =  value;
//       this.messages = i18nMessages[this.locale];
//       test.lang = value;
//     }
//   }
//   
//   isSupported (lang) {
//     return SUPPORTED_LANG.includes(lang);
//   }
//   
//   t (id) {
//     return this.messages[id] ? this.messages[id] : '';
//   }
// }

// export default Locale;