// @flow
const toRad = function (num) {
  return num * Math.PI / 180;
}

/**
 * Calculate distance
 * @param  {[number, number]} start [latitude, longitude]
 * @param  {[number, number]} end   [latitude, longitude]
 * @return {number}       Distance.
 */
export function calculateDistance (start, end) {
  const R = 6371;

  const dLat = toRad(end[0] - start[0]);
  const dLon = toRad(end[1] - start[1]);
  const lat1 = toRad(start[0]);
  const lat2 = toRad(end[0]);

  const a = Math.sin(dLat/2) * Math.sin(dLat/2) +
          Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2);
  const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

  return R * c;
}

const SECOND = 1;
const MINUTE = 60 * SECOND;
const HOUR = 60 * MINUTE;

export function formatTime (time) {
  const timeParts = [HOUR, MINUTE, SECOND].map((i) => {
    const v = Math.floor(time / i);
    time -= v * i;
    return v < 10 ? `0${v}` : v;
  });
  
  return `${timeParts[0]}:${timeParts[1]}:${timeParts[2]}`;
}

export function formatDate (date) {
  if (typeof date === 'string') {
    date = new Date(date);
  }
  
  return `${date.getDay()}/${date.getMonth() + 1}/${date.getFullYear()}`;
}