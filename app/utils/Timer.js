import EventEmitter from 'event-emitter';

const UPDATE_INTERVAL = 1000; // Time in ms.

class Timer extends EventEmitter {
  timePassed = 0;
  inited = false;
  
  start = () => {
    if (!this.inited) {
      this.startAt = new Date();
    }
    this.prevTime = (new Date()).getTime();
    this.timerId = setTimeout(this.updateTime, UPDATE_INTERVAL);
    this.emit('started');
  }
  
  stop = () => {
    clearTimeout(this.timerId);
    this.timerId = null;
    this.emit('stopped');
  }
  
  updateTime = () => {
    this.timePassed = this.timePassed + Math.round(((new Date()).getTime() - this.prevTime) / 1000);
    this.emit('timeChanged', this.timePassed);
    this.timerId = setTimeout(this.updateTime, UPDATE_INTERVAL);
    this.prevTime = (new Date()).getTime();
  }
}

export default Timer;