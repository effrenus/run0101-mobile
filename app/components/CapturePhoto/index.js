import React, { Component } from 'react';
import { View, Modal, Text } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Camera from 'react-native-camera';
import styles from './styles';

class CapturePhoto extends Component {
  state = {
    isModalVisible: false
  };
  
  toggleState = () => this.setState({ isModalVisible: !this.state.isModalVisible })
  
  takePicture = () => {
    this.camera.capture()
      .then(({ path }) => {
        this.props.onCapture && this.props.onCapture(path);
        this.toggleState();
      })
      .catch(err => console.log(err));
  }
  
  render () {
    return (
      <View style={{ flex: 1 }}>
        <Modal
          transparent={false}
          onRequestClose={() => {}}
          visible={this.state.isModalVisible}>
          <Camera
            ref={(cam) => {
              this.camera = cam;
            }}
            style={{ flex: 1, marginBottom: 100 }}
            aspect={Camera.constants.Aspect.fill}>
            <View style={{ flex: 1 }}>
              <Icon name="close" onPress={this.toggleState} size={45} color="#FFF" style={styles.iconClose} />
              <Icon name="photo-camera" onPress={this.takePicture} size={65} color="#FFF" style={styles.iconPhoto} />
            </View>
          </Camera>
        </Modal>
        
        <View style={{ flexDirection: 'column', alignItems: 'center' }}>
          <Icon name="add-a-photo" size={40} onPress={this.toggleState} />
        </View>
      </View>
    );
  }
}

export default CapturePhoto;