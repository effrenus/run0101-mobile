import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  iconClose: {
    position: 'absolute', 
    top: 5, 
    right: 5
  },
  iconPhoto: {
    position: 'absolute', 
    bottom: 10, 
    left: 130
  }
})