import React from 'react';
import { StyleSheet, Text } from 'react-native';
import * as m from 'app/config';

export const H1 = (props) =>
  <Text style={[styles.base, styles.h1]}>
    {props.children}
  </Text>;

export const H2 = (props) =>
  <Text style={[styles.base, styles.h2]}>
    {props.children}
  </Text>;

export const P = (props) =>
  <Text style={[styles.base, styles.p]}>
    {props.children}
  </Text>;

const styles = StyleSheet.create({
  base: {
    color: m.TEXT_COLOR,
    fontSize: m.BASE_FONT_SIZE
  },
  h1: {
    fontSize: m.BASE_FONT_SIZE * 1.7,
    marginBottom: m.BASE_FONT_SIZE
  },
  h2: {
    fontSize: m.BASE_FONT_SIZE * 1.5,
    marginBottom: m.BASE_FONT_SIZE * 0.5
  },
  p: {
    marginBottom: m.BASE_FONT_SIZE,
    lineHeight: Math.ceil(m.BASE_FONT_SIZE * 1.55)
  }
});