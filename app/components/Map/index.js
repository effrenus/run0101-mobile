import React, { Component } from 'react';
import WebViewBridge from 'react-native-webview-bridge';
import { View } from 'react-native';

class Map extends Component {
  mapLoaded = false;
  
  componentWillReceiveProps (nextProps) {
    const nextCoords = nextProps.coords.toJS();
    this.sendToView({ type: 'setCoordinates', payload: nextCoords });
  }
  
  shouldComponentUpdate () {
    return false;
  }
  
  onBridgeMessage = (eventString) => {
    const event = JSON.parse(eventString);
    if (event.type === 'mapready') {
      this.onMapReady();
    }
  }
  
  onMapReady () {
    this.mapLoaded = true;
    navigator.geolocation.getCurrentPosition(
      ({ coords: { latitude, longitude } }) => this.sendToView({ type: 'setCenter', payload: [latitude, longitude] })
    );
  }
  
  sendToView (action) {
    const { bridge } = this.refs;
    bridge.sendToBridge(JSON.stringify(action));
  }
  
  render () {
    return (
      <View style={{ flex: 1 }}>
        <WebViewBridge
          ref="bridge"
          onBridgeMessage={this.onBridgeMessage}
          source={{ uri: 'file:///android_asset/Webview.html' }}
        />
      </View>
    );
  }
}

export default Map;