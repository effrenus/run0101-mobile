// @flow
import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import SettingSelector from './SettingSelector';
import { BASE_FONT_SIZE } from 'app/config';

import type { SelectorValue } from './SettingSelector';
import type { Navigator } from 'react-native';

class SettingsField extends Component {
  props: {
    navigator: Navigator,
    name: string,
    defaultValue: SelectorValue,
    onChange?: (value: SelectorValue) => void,
    values: Array<{val: SelectorValue, name: string}>
  };
  
  state: {value: SelectorValue | null} = {
    value: null
  };
  
  defaultProps = {
    defaultValue: ''
  };
  
  handleChange = (value: SelectorValue) => {
    this.setState(() => ({ value }));
    this.props.onChange && this.props.onChange(value)
  }
  
  handlePress = () => {
    const { navigator, values, defaultValue } = this.props;
    navigator.push({
      component: SettingSelector,
      passProps: {
        onChange: this.handleChange,
        values,
        defaultValue: this.state.value || defaultValue
      }
    });
  }
  
  render () {
    const { name, defaultValue } = this.props;
    
    return (
      <TouchableOpacity onPress={this.handlePress}>
      <View style={styles.container}>
        <Text style={[styles.defaultText]}>{name}</Text>
        <View>
          <Icon name="keyboard-arrow-right" size={30} /> 
        </View>
      </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    paddingHorizontal: 15,
    paddingVertical: 10,
    alignItems: 'center',
    justifyContent: 'space-between',
    borderBottomWidth: 1,
    borderBottomColor: '#DDD'
  },
  defaultText: {
    fontSize: BASE_FONT_SIZE * 1.2
  }
});

export default SettingsField;