// @flow
import React, { Component } from 'react';
import { View, Text, TouchableWithoutFeedback } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import * as config from 'app/config';

export type SelectorValue = string | number;

class SettingSelector extends Component {
  props: {
    defaultValue: SelectorValue,
    onChange?: (value: SelectorValue) => void,
    values: Array<{val: SelectorValue, name: string}>
  };
  
  state: { value: ?SelectorValue } = {
    value: null
  };
  
  handlePress = (value: string | number) => {
    if (value !== this.props.value){
      this.setState(() => ({ value }));
      this.props.onChange && this.props.onChange(value);
    }
  }
  
  render () {
    const { values } = this.props;
    const currentValue = this.state.value ? this.state.value : this.props.defaultValue;
    
    return (
      <View style={{ flex: 1, backgroundColor: '#edeaea' }}>
        <Icon.ToolbarAndroid
        title="Back"
        actions={[]}
        style={{ height: config.TOOLBAR_HEIGHT, backgroundColor: config.BLUE_COLOR }}
        titleColor="#FFF"
        navIconName="arrow-back"
        onIconClicked={() => this.props.navigator.pop()}
        />
        {values.map((value, i) => 
          <TouchableWithoutFeedback key={i} onPress={this.handlePress.bind(this, value.val)}>
          <View style={{ flexDirection: 'row', justifyContent: 'space-between', backgroundColor: '#FFF', padding: 17, borderBottomWidth: 1, borderBottomColor: '#DDD' }}>
            <Text style={{ fontSize: 21 }}>{value.name}</Text>
            {value.val === currentValue ? <Icon name="check" size={22} color="green" /> : null}
          </View>
          </TouchableWithoutFeedback>
        )}
      </View>
    );
  }
}

export default SettingSelector;