import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet
} from 'react-native';

class Field extends Component {
  render () {
    const { label, value, unit } = this.props;
    
    return (
      <View style={styles.field}>
        <Text style={styles.label}>{label}</Text>
        <Text style={styles.value}>{value}{unit ? <Text style={styles.unit}> {unit.toUpperCase()}</Text> : ''}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  label: {
    fontSize: 18
  },
  field: {
    marginBottom: 10,
    paddingLeft: 10
  },
  value: {
    fontSize: 37,
    color: '#000000'
  },
  unit: {
    fontSize: 18
  }
});

export default Field;