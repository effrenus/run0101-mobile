// @flow
import React from 'react';
import { View, Image, Text } from 'react-native';

const SpashScreen = () =>
  <View style={{ flex: 1 }}>
    <Image source={require('../images/splash.png')} style={{ flex: 1, resizeMode: 'cover', width: null, height: null }} />
    <View style={{ position: 'absolute', left: 0, top: 0, bottom: 0, right: 0, flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }}>
      <Text style={{ fontSize: 40, fontFamily: 'Lobster', color: '#333' }}>Run 1st january</Text>
    </View>
  </View>
  
  export default SpashScreen;