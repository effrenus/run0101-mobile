import React, { Component, PropTypes } from 'react';
import { DrawerLayoutAndroid } from 'react-native';

class DrawerLayout extends Component {
  getChildContext () {
    return {
      openDrawer: this.openDrawer.bind(this)
    };
  }
  
  openDrawer () {
    this._drawer.openDrawer();
  }
  
  closeDrawer () {
    this._drawer.closeDrawer();
  }
  
  render () {
    return (
      <DrawerLayoutAndroid
        ref={(ref) => { this._drawer = ref; }}
        drawerWidth={250}
        drawerPosition={DrawerLayoutAndroid.positions.Left}
        renderNavigationView={this.props.renderNavigationView}
      >
        {this.props.children}
      </DrawerLayoutAndroid>
    );
  }
}

DrawerLayout.childContextTypes = {
  openDrawer: PropTypes.func
};

export default DrawerLayout;