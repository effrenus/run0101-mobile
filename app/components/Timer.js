import React, { Component } from 'react';
import { observer } from 'mobx-react/native';
import Field from './Field';
import { formatTime } from 'app/utils';

@observer
class Timer extends Component {
  render () {
    const { activity } = this.props;
    return (
      <Field label={this.props.label} value={formatTime(activity.duration)} />
    );
  }
}

export default Timer;