export const TOOLBAR_HEIGHT = 55;

export const BLUE_COLOR = '#145493';

export const TEXT_COLOR = '#000';

export const BASE_FONT_SIZE = 17;

export const SUPPORTED_LANG = ['ru', 'en'];