// @flow
import React, { Component } from 'react';
import { View, AppState } from 'react-native';
import { Provider } from 'mobx-react';
import { observer } from 'mobx-react/native';
import Navigator from 'app/Navigator';
import SplashScreen from 'app/components/SplashScreen';
import appstate from 'app/stores/index';

const rehydrateStore = async () => {
  await appstate.loadStorage();
}

@observer
class App extends Component {
  state = {
    rehydrated: false
  };
  
  async componentDidMount () {
    rehydrateStore().then(() => this.setState(() => ({ rehydrated: true })));
  }
  
  componentWillMount () {
    AppState.addEventListener('change', appstate.handleStateChange);
  }
  
  componentWillUnmount () {
    AppState.removeEventListener('change', appstate.handleStateChange);
  }
  
  renderApp () {
    return (
      <Provider appstate={appstate}>
      <View style={{ flex: 1 }}>
        <Navigator />
      </View>
      </Provider>
    );
  }
  
  render () {
    return this.state.rehydrated
        ? this.renderApp()
        : <SplashScreen />;
  }
}

export default App;