package com.run0101mobile;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.HeadlessJsTaskService;
import com.facebook.react.jstasks.HeadlessJsTaskConfig;
import com.facebook.react.ReactApplication;
import com.facebook.react.ReactInstanceManager;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.common.LifecycleState;

public class ActivityService extends HeadlessJsTaskService {
    @Override
    protected HeadlessJsTaskConfig getTaskConfig(Intent intent) {
        Bundle extras = intent.getExtras();
        String jobKey = extras.getString("jobKey");

        if( isAppInForeground() ){
          stopSelf();
          return null;
        }
        return new HeadlessJsTaskConfig(
              jobKey,
              Arguments.fromBundle(extras),
              5000,
              true
            );
    }

    private boolean isAppInForeground() {
        final ReactInstanceManager reactInstanceManager =
                ((ReactApplication) getApplication())
                        .getReactNativeHost()
                        .getReactInstanceManager();
        ReactContext reactContext =
          reactInstanceManager.getCurrentReactContext();

        return(reactContext != null && reactContext.getLifecycleState() == LifecycleState.RESUMED);
    }
}