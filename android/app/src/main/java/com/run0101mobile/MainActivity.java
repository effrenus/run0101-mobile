package com.run0101mobile;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.facebook.react.ReactActivity;

public class MainActivity extends ReactActivity {

    /**
     * Returns the name of the main component registered from JavaScript.
     * This is used to schedule rendering of the component.
     */
    @Override
    protected String getMainComponentName() {
        return "run0101mobile";
    }
    
    @Override
    protected void onPause() {
      super.onPause();
    }
    
    @Override
    protected void onResume() {
      super.onResume();
    }
}
